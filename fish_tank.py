def neo1():
    global rnd
    rnd = randint(1, 4)
    basic.show_number(rnd)
    if rnd == 1:
        strip.show_color(neopixel.colors(NeoPixelColors.RED))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.GREEN))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.BLUE))
    elif rnd == 2:
        strip.show_color(neopixel.colors(NeoPixelColors.INDIGO))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.PURPLE))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.WHITE))
    elif rnd == 3:
        strip.show_color(neopixel.colors(NeoPixelColors.WHITE))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.ORANGE))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.BLACK))
    else:
        strip.show_rainbow(1, 360)
        basic.pause(randint(800, 3000))
    basic.pause(randint(200, 800))

def on_on_event():
    global rndpump
    rndpump = randint(1, 5)
    if rndpump == 1:
        pins.digital_write_pin(DigitalPin.P1, 1)
        pins.digital_write_pin(DigitalPin.P2, 1)
        pins.digital_write_pin(DigitalPin.P13, 1)
        pins.digital_write_pin(DigitalPin.P8, 0)
        pins.digital_write_pin(DigitalPin.P16, 0)
        basic.pause(randint(300, 3000))
    elif rndpump == 2:
        pins.digital_write_pin(DigitalPin.P16, 1)
        pins.digital_write_pin(DigitalPin.P8, 1)
        pins.digital_write_pin(DigitalPin.P2, 0)
        pins.digital_write_pin(DigitalPin.P1, 0)
        basic.pause(randint(300, 3000))
    elif rndpump == 3:
        pumpAllOff()
        pins.digital_write_pin(DigitalPin.P1, 1)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P2, 1)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P13, 1)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P8, 1)
        pins.digital_write_pin(DigitalPin.P16, 1)
        basic.pause(randint(300, 1000))
    elif rndpump == 4:
        pumpAllOn()
        pins.digital_write_pin(DigitalPin.P8, 0)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P2, 0)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P1, 0)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P13, 0)
        basic.pause(randint(300, 1000))
        pins.digital_write_pin(DigitalPin.P16, 0)
    else:
        for index in range(randint(10, 20)):
            pumpAllOn()
            basic.pause(randint(300, 1000))
            pins.digital_write_pin(DigitalPin.P2, 0)
            pins.digital_write_pin(DigitalPin.P8, 0)
            pumpAllOff()
            basic.pause(randint(300, 1000))
            pins.digital_write_pin(DigitalPin.P1, 1)
            pins.digital_write_pin(DigitalPin.P16, 1)
            basic.pause(randint(300, 1000))
    basic.pause(randint(300, 500))
    control.raise_event(4800, 4801)
control.on_event(4800, 4801, on_on_event)

def neo4():
    global rnd
    strip.show_color(neopixel.colors(NeoPixelColors.WHITE))
    string.show_color(neopixel.colors(NeoPixelColors.GREEN))
    brd12.show_color(neopixel.colors(NeoPixelColors.BLUE))
    rng1.show_color(neopixel.colors(NeoPixelColors.VIOLET))
    rng2.show_color(neopixel.colors(NeoPixelColors.PURPLE))
    rnd = randint(20, 100)
    for index2 in range(randint(500, 2000)):
        strip.rotate(1)
        strip.show()
        basic.pause(rnd)
def neo2():
    global rnd
    rnd = randint(1, 4)
    basic.show_number(rnd)
    if rnd == 1:
        strip.show_color(neopixel.colors(NeoPixelColors.BLACK))
        basic.pause(randint(200, 800))
        string.show_color(neopixel.rgb(randint(100, 200), randint(100, 200), randint(100, 200)))
        basic.pause(randint(200, 800))
        brd1.show_color(neopixel.rgb(randint(100, 200), randint(100, 200), randint(100, 200)))
        basic.pause(randint(200, 800))
        brd2.show_color(neopixel.rgb(randint(100, 200), randint(100, 200), randint(100, 200)))
        basic.pause(randint(200, 800))
        rng1.show_color(neopixel.rgb(randint(100, 200), randint(100, 200), randint(100, 200)))
        basic.pause(randint(200, 800))
        rng2.show_color(neopixel.rgb(randint(100, 200), randint(100, 200), randint(100, 200)))
    elif rnd == 2:
        strip.show_color(neopixel.colors(NeoPixelColors.RED))
        basic.pause(randint(200, 800))
        strip.show_color(neopixel.colors(NeoPixelColors.GREEN))
        basic.pause(randint(200, 800))
        brd12.show_color(neopixel.colors(NeoPixelColors.BLUE))
        rng12.show_color(neopixel.colors(NeoPixelColors.PURPLE))
        basic.pause(randint(200, 800))
        string.show_color(neopixel.colors(NeoPixelColors.INDIGO))
    elif rnd == 3:
        strip.show_rainbow(1, 360)
        rnd = randint(20, 80)
        for index3 in range(randint(300, 1000)):
            strip.rotate(1)
            strip.show()
            basic.pause(rnd)
    else:
        strip.show_color(neopixel.colors(NeoPixelColors.RED))
        brd12.show_color(neopixel.colors(NeoPixelColors.YELLOW))
        rng1.show_color(neopixel.colors(NeoPixelColors.GREEN))
        rng2.show_color(neopixel.colors(NeoPixelColors.BLUE))
        rnd = randint(20, 80)
        for index4 in range(randint(300, 1000)):
            strip.rotate(1)
            strip.show()
            basic.pause(rnd)
    basic.pause(randint(200, 800))
def neo3():
    global rnd
    strip.show_color(neopixel.colors(NeoPixelColors.RED))
    basic.pause(randint(200, 800))
    strip.show_color(neopixel.colors(NeoPixelColors.GREEN))
    basic.pause(randint(200, 800))
    strip.show_color(neopixel.colors(NeoPixelColors.BLUE))
    basic.pause(randint(200, 800))
    rnd = randint(1, 3)
    basic.show_number(rnd)
    if rnd == 1:
        rng1.show_color(neopixel.colors(NeoPixelColors.VIOLET))
        rng1.show_color(neopixel.colors(NeoPixelColors.INDIGO))
    elif rnd == 2:
        strip.show_rainbow(1, 360)
        basic.pause(randint(2000, 4000))
    else:
        brd1.show_color(neopixel.colors(NeoPixelColors.WHITE))
        brd1.show_color(neopixel.colors(NeoPixelColors.BLUE))
    basic.pause(randint(200, 800))
def pumpAllOn():
    pins.digital_write_pin(DigitalPin.P1, 1)
    pins.digital_write_pin(DigitalPin.P2, 1)
    pins.digital_write_pin(DigitalPin.P13, 1)
    pins.digital_write_pin(DigitalPin.P8, 1)
    pins.digital_write_pin(DigitalPin.P16, 1)
def pumpAllOff():
    pins.digital_write_pin(DigitalPin.P1, 0)
    pins.digital_write_pin(DigitalPin.P2, 0)
    pins.digital_write_pin(DigitalPin.P13, 0)
    pins.digital_write_pin(DigitalPin.P8, 0)
    pins.digital_write_pin(DigitalPin.P16, 0)
rndpump = 0
rnd = 0
rng12: neopixel.Strip = None
rng2: neopixel.Strip = None
rng1: neopixel.Strip = None
brd12: neopixel.Strip = None
brd2: neopixel.Strip = None
brd1: neopixel.Strip = None
string: neopixel.Strip = None
strip: neopixel.Strip = None
led.set_brightness(58)
strip = neopixel.create(DigitalPin.P0, 78, NeoPixelMode.RGB)
string = strip.range(0, 30)
brd1 = strip.range(30, 8)
brd2 = strip.range(38, 8)
brd12 = strip.range(30, 16)
rng1 = strip.range(46, 16)
rng2 = strip.range(62, 16)
rng12 = strip.range(46, 32)
for index5 in range(10):
    pumpAllOn()
    basic.pause(randint(300, 3000))
    pumpAllOff()
    basic.pause(randint(300, 3000))
control.raise_event(4800, 4801)

def on_forever():
    global rnd
    rnd = randint(1, 4)
    if rnd == 1:
        basic.show_string("A")
        basic.pause(200)
        neo1()
    elif rnd == 2:
        basic.show_string("B")
        basic.pause(200)
        neo2()
    elif rnd == 3:
        basic.show_string("C")
        basic.pause(200)
        neo3()
    else:
        basic.show_string("D")
        basic.pause(200)
        neo4()
basic.forever(on_forever)
